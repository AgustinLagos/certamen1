from django.contrib import admin
from .models import *
from .forms import EstudianteForm
# Register your models here.

class AdminRegistrado(admin.ModelAdmin):
    list_display = ["id_registro","nombre_estudiante","fecha_de_ingreso"]
    list_filter = ["fecha_de_ingreso"]
    form = EstudianteForm()
    class Meta:
        model = RegistroEstudiante


admin.site.register(RegistroEstudiante,AdminRegistrado)