from django import forms
from .models import RegistroEstudiante

class EstudianteForm(forms.Form):
    nombre_estudiante = forms.CharField(max_length=100)
    edad_estudiante = forms.IntegerField(null=False)
    rut_estudiante = forms.IntegerField()
    carrera_estudiante = forms.CharField(max_length=100)
    fecha_de_ingreso = forms.DateTimeField()
    class Meta:
        modelo = RegistroEstudiante
        campos = ["nombre_estudiante","edad_estudiante","rut_estudiante","carrera_estudiante","fecha_de_ingreso"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre_estudiante")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_edad(self):
        edad = self.cleaned_data.get("edad_estudiante")
        if edad==0:
            raise forms.ValidationError("La edad no puede ser 0")
        elif edad < 0:
            raise forms.ValidationError("La edad no puede negativa")
        elif edad < 17:
            raise forms.ValidationError("La edad no puede ser menor a 17")

    def clean_carrera(self):
        carrera = self.cleaned_data.get("carrera_estudiante")
        if carrera:
            if len(carrera) <= 1:
                raise forms.ValidationError("El campo nombre no puede ser de un caracter")
            return carrera
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

