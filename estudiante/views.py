from django.shortcuts import render
from .forms import EstudianteForm
from .models import RegistroEstudiante
# Create your views here.

def estudiante(request):
    form = EstudianteForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre_estudiante")
        edad2 = form_data.get("edad_estudiante")
        rut2 = form_data.get("rut_estudiante")
        carrera2 = form_data.get("carrera_estudiante")
        fecha2 = form_data.get("fecha_de_ingreso")

        objeto = RegistroEstudiante.objects.create(nombre_estudiante=nombre2,edad_estudiante=edad2,rut_estudiante=rut2,
                                                   carrera_estudiante=carrera2,fecha_de_ingreso=fecha2)
    contexto = {"formulario_estudiante":form,}

    return  render(request, "estudiante.html",contexto)