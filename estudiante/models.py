from django.db import models

# Create your models here.

class RegistroEstudiante(models.Model):
    id_registro = models.AutoField(primary_key=True, unique=True)
    nombre_estudiante = models.CharField(max_length=100)
    edad_estudiante = models.IntegerField(null=False,blank=False)
    rut_estudiante = models.IntegerField(null=False,blank=False)
    carrera_estudiante = models.CharField(max_length=100)
    fecha_de_ingreso = models.DateTimeField()

    def __str__(self):
        return self.nombre_estudiante