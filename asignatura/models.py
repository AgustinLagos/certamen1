from django.db import models

# Create your models here.
class RegistroAsignatura(models.Model):
    id_asignatura = models.AutoField(primary_key=True,unique=True)
    nombre_asignatura = models.CharField(max_length=100)
    nombre_carrera = models.CharField(max_length=100)
    semestre = models.IntegerField(null=False,blank=False)
    fecha_incorporacion = models.DateTimeField()

    def __str__(self):
        return self.nombre_asignatura

