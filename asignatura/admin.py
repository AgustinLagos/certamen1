from django.contrib import admin
from .models import *
# Register your models here.

class AdminAsignatura(admin.ModelAdmin):
    list_display = ["id_asignatura","nombre_asignatura","fecha_incorporacion"]
    list_filter = ["fecha_incorporacion"]
    class Meta:
        model = RegistroAsignatura

admin.site.register(RegistroAsignatura,AdminAsignatura)