from django.db import models

# Create your models here.

class RegistroFacultades(models.Model):
    id_registro = models.AutoField(primary_key=True, unique=True)
    nombre_facultad = models.CharField(max_length=100)
    descripcion = models.TextField()
    ubicacion = models.TextField()
    minimo_departamentos = models.IntegerField()

    def __str__(self):
        return self.nombre_facultad