from django.contrib import admin
from .models import *
from .forms import FormCarrera

# Register your models here.

class AdminCarrera(admin.ModelAdmin):
    list_display = ["nombre_carrera","id_carrera","facultad","fecha_apertura"]
    list_filter = ["fecha_apertura"]
    form = FormCarrera
    class Meta:
        model = RegistroCarrera





admin.site.register(RegistroCarrera,AdminCarrera)
