from django.db import models

# Create your models here.

class RegistroCarrera(models.Model):
    id_carrera=models.AutoField(primary_key=True,unique=True)
    nombre_carrera = models.CharField(max_length=100)
    descripcion_carrera = models.TextField()
    estudiantes_minimos = models.IntegerField(null=False,blank=False)
    facultad = models.CharField(max_length=100)
    fecha_apertura = models.DateTimeField()

    def __str__(self):
        return self.nombre_carrera
