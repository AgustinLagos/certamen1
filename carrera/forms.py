from django import forms
from .models import *
class FormCarrera(forms.Form):
    nombre_carrera = forms.CharField(max_length=100)
    descripcion_carrera = forms.TextInput()
    estudiantes_minimos = forms.IntegerField(null=False, blank=False)
    facultad = forms.CharField(max_length=100)
    fecha_apertura = forms.DateTimeField()

    class Meta:
        modelo = RegistroCarrera
        campos = ["nombre_carrera","descripcion_carrera","estudiantes_minimos ","facultad","fecha_apertura"]

    def clean_nombre_carrera(self):
        nombre = self.cleaned_data.get("nombre_carrera")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    